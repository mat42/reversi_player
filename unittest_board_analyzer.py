import unittest
from board_analyzer import BoardAnalyzer
from field import Field


class BoardAnalyzerTest(unittest.TestCase):
    #          0   1   2   3   4   5   6   7
    board = [[-1, -1, -1, -1, -1, -1, -1, -1],  # 0
             [-1, -1, -1, -1, -1, -1, -1, -1],  # 1
             [-1, -1, -1, +1, -1, -1, -1, -1],  # 2
             [-1, -1, -1, +0, +1, +1, +1, -1],  # 3
             [-1, -1, -1, +0, +1, -1, -1, -1],  # 4
             [-1, -1, -1, +0, +1, -1, -1, -1],  # 5
             [-1, -1, -1, -1, -1, -1, -1, -1],  # 6
             [-1, -1, -1, -1, -1, -1, -1, -1]]  # 7

    moves_map_expected_1 = {
        (2, 2): [(3, 3)],
        (2, 3): [(3, 3), (3, 4)],
        (2, 4): [(3, 4)],
        (2, 5): [(3, 4), (3, 5)],
        (2, 6): [(3, 5)],
        (3, 6): [(3, 3), (3, 4), (3, 5)]}

    moves_map_expected_0 = {
        (5, 2): [(4, 3)],
        (6, 2): [(5, 3), (4, 4)],
        (7, 3): [(4, 3), (5, 3), (6, 3)],
        (5, 4): [(4, 4)],
        (5, 5): [(4, 4), (4, 5)],
        (5, 6): [(4, 5)],
        (3, 1): [(3, 2)]
    }

    def map_board(self, raw_board):
        mapped_board = []
        for y, row in enumerate(raw_board):

            mapped_row = []
            for x, field in enumerate(row):
                mapped_row.append(Field(field, x, y))

            mapped_board.append(mapped_row)

        return mapped_board

    def test_find_moves_and_rows(self):
        Field.my_colour = 1
        Field.opponent_colour = 0
        board_analyzer = BoardAnalyzer()

        moves = board_analyzer.get_available_opponent_moves(self.map_board(self.board))

        self.assertCountEqual(map(lambda m: m.loc, moves), self.moves_map_expected_0.keys())

        for exp_move, exp_row in self.moves_map_expected_0.items():
            row = [move.row for move in moves if move.loc == exp_move]
            self.assertSetEqual(set(row[0]), set(exp_row))
