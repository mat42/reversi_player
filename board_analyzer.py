from field import Field
from structs import Move

class BoardAnalyzer:

    def __init__(self):
        self.board = []
        self.moves_dict = {}


    def add_move_on_opponent_row(self, row):
        
        move_loc = row[-1]
        move = self.moves_dict.get(move_loc)

        if move is not None:
            move.row += row[:-1]

        elif self.board[move_loc[1]][move_loc[0]].get_is_empty():
            self.moves_dict[move_loc] = Move(move_loc, row[:-1])


    def gen_opponent_row_loc(self, start_field, direction):

        x, y = start_field.x, start_field.y
        
        while True:
            field = self.board[y][x]

            if not field.get_is_my_stone():
                yield x, y

            x += direction[0]
            y += direction[1]

            if not field.get_is_opp_stone()\
             or x >= len(self.board[0]) or x < 0\
             or y >= len(self.board) or y < 0:
                break


    def gen_my_stones(self):
        for row in self.board:
            for field in row:
                if field.get_is_my_stone():
                    yield field


    def gen_neighbours(self, field):
        
        #against negative indexes
        start_y = field.y - 1 if field.y >= 1 else field.y
        start_x = field.x - 1 if field.x >= 1 else field.x

        #slising automatically checks borders
        for horisontal in self.board[start_y:(field.y + 2)]:
            for nhb_field in horisontal[start_x:(field.x + 2)]:
                if nhb_field != field:
                    yield nhb_field


    def get_available_moves(self, board):
        self.moves_dict.clear()
        self.board = board

        for my_stone in self.gen_my_stones():
            for field in self.gen_neighbours(my_stone):
                if field.get_is_opp_stone():
                    direction = (field.x - my_stone.x, field.y - my_stone.y)
                    row = list(self.gen_opponent_row_loc(field, direction))
                    self.add_move_on_opponent_row(row)

        return list(self.moves_dict.values())


    def get_available_opp_moves(self, board):
        Field.swap_colours()
        available_moves = self.get_available_moves(board)
        Field.swap_colours()
        return available_moves

