from field import Field
from structs import Board
from game_tree_builder import GameTreeBuilder


class MyPlayer:
    '''Alpha-betta player. Checks only moves with maximal profit on next step.'''

    def __init__(self, my_colour, opponent_colour):

        self.name = 'buninmat'

        Field.my_colour = my_colour
        Field.opponent_colour = opponent_colour

        self.game_tree_builder = GameTreeBuilder()

    def map_board(self, raw_board):
        mapped_board = []
        for y, row in enumerate(raw_board):

            mapped_row = []
            for x, field in enumerate(row):
                mapped_row.append(Field(field, x, y))

            mapped_board.append(mapped_row)

        return mapped_board

    def move(self, raw_board):

        board_map = self.map_board(raw_board)
        board = Board(board_map)

        corresp_board = self.game_tree_builder.boards.get(board.hash, None)
        if corresp_board is None:
            self.game_tree_builder.build_game_tree(board, 3)

        next_move = self.game_tree_builder.boards.get(
            board.hash, None).best_move
        return (next_move.loc[1], next_move.loc[0])
