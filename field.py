class Field:
    my_colour = None
    opponent_colour = None

    def __init__(self, value, x=None, y=None):
        self.x, self.y = x, y
        self.value = value

    def get_is_my_stone(self):
        return self.value == self.my_colour

    def get_is_opp_stone(self):
        return self.value == self.opponent_colour

    def get_is_empty(self):
        return not self.get_is_my_stone() and not self.get_is_opp_stone()

    @staticmethod
    def swap_colours():
        Field.my_colour, Field.opponent_colour = Field.opponent_colour, Field.my_colour

    def __eq__(self, obj):
        return obj.x == self.x\
            and obj.y == self.y\
            and obj.value == self.value

    def __repr__(self):

        field_type = 'none'

        if self.get_is_my_stone():
            field_type = 'my'
        if self.get_is_opp_stone():
            field_type = 'opponent`s'

        return f'({self.x},{self.y}): {field_type}'
