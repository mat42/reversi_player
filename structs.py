class Board:

    def calculate_hash(self):
        hash = 0
        pow_counter = 0
        for row in self.matrix:
            for field in row:
                if field.get_is_my_stone():
                    hash += 2 ** pow_counter
                if field.get_is_opp_stone():
                    hash += 2 ** (65 + pow_counter)
                pow_counter += 1
        return hash

    def __init__(self, matrix):
        self.matrix = matrix
        self.best_move = None
        self.hash = self.calculate_hash()


class Move:
    def __init__(self, loc, row, value=0):
        self.row = row
        self.loc = loc
        self.value = value
