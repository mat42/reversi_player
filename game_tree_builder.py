from board_analyzer import BoardAnalyzer
from structs import Board
from field import Field
from copy import deepcopy

class GameTreeBuilder:

    def __init__(self):
        self.board_analyzer = BoardAnalyzer()
        self.boards = {}
        self.depth = 0


    def get_board_after_move(self, move, is_my_move, board):
        colour = Field.my_colour if is_my_move else Field.opponent_colour
        new_board_matrix = deepcopy(board.matrix)
        for opponent_loc in move.row + [move.loc]:
            new_board_matrix[opponent_loc[1]][opponent_loc[0]].value = colour
        new_board = Board(new_board_matrix)
        return new_board

    def evaluate_move(self, move, is_my_move):
        value = (len(move.row) + 1) * (1 if is_my_move else -1)
        #moves to ends of board value more
        if move.loc[0] == 0 or move.loc[1] == 0:
            value += 1
        if move.loc[0] == 7 or move.loc[1] == 7:
            value += 1
        return value

    def gen_moves_with_max_value(self, moves):

        max_value = 0
        for move in moves:
            move.value = self.evaluate_move(move, True)
            if move.value > max_value:
                max_value = move.value
        
        return filter(lambda m: m.value == max_value, moves)


    def calculate_game(self, board, is_my_move, curr_depth = 0, curr_value = 0):

        if curr_depth >= self.depth:
            return curr_value

        moves = self.board_analyzer.get_available_moves(board.matrix) if is_my_move\
                else self.board_analyzer.get_available_opp_moves(board.matrix)
        
        if not moves:
            return self.calculate_game(board, is_my_move, curr_depth + 1)
        
        if is_my_move:
            self.boards[board.hash] = board
        
        moves_to_check = self.gen_moves_with_max_value(moves) if is_my_move else moves

        for move in moves_to_check:
            next_board = self.get_board_after_move(move, is_my_move, board)
            prev_value = move.value if is_my_move else self.evaluate_move(move, False)
            move.value = self.calculate_game(next_board, not is_my_move, curr_depth + 1, prev_value)
        
        best_move = (max if is_my_move else min)(moves, key=lambda move: move.value)
        if is_my_move:
            self.boards[board.hash].best_move = best_move

        return curr_value + best_move.value


    def build_game_tree(self, board, depth):
        self.depth = depth
        self.calculate_game(board, is_my_move=True)
        pass
